/*
 * Replace the following string of 0s with your student number
 * 210417616
 */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "job.h"

/*
 * DO NOT EDIT the job_new function.
 */
job_t *job_new(pid_t pid, unsigned int id, unsigned int priority, const char *label) {
    return job_set((job_t *)malloc(sizeof(job_t)), pid, id, priority, label);
}

/*
 * TODO: you must implement this function
 */
job_t *job_copy(job_t *src, job_t *dst) {
    if ((!src || strnlen(src->label, MAX_NAME_SIZE) != MAX_NAME_SIZE - 1)) { return NULL; }
    if (!dst) { dst = job_new(src->pid, src->id, src->priority, src->label); }
    if (dst && (src != dst)) { job_set(dst, src->pid, src->id, src->priority, src->label); }
    return dst;
}

/*
 * TODO: you must implement this function
 */
void job_init(job_t *job) {
    if (job) {
        job->pid = 0;
        job->id = 0;
        job->priority = 0;
        snprintf(job->label, MAX_NAME_SIZE, PAD_STRING);
    }
}

/*
 * TODO: you must implement this function
 */
bool job_is_equal(job_t *j1, job_t *j2) {
    if ((!j1 && !j2) || ((j1 && j2) && (j1->pid == j2->pid) && (j1->id == j2->id) && (j1->priority == j2->priority) && (strncmp(j1->label, j2->label, MAX_NAME_SIZE) == 0))) {
        return true;
    }
    return false;
}

/*
 * TODO: you must implement this function.
 * Hint:
 * - read the information in job.h about padding and truncation of job
 *      labels
 */
job_t *job_set(job_t *job, pid_t pid, unsigned int id, unsigned int priority, const char *label) {
    if (!job) { return NULL; }
    job->pid = pid; 
    job->id = id; 
    job->priority = priority; 
    if (label) { 
        snprintf(job->label, MAX_NAME_SIZE, "%s%s", label, PAD_STRING); 
    } else {
        snprintf(job->label, MAX_NAME_SIZE, "%s", PAD_STRING); 
    }
    return job;
}

/*
 * TODO: you must implement this function.
 * Hint:
 * - see malloc and calloc system library functions for dynamic allocation,
 *      and the documentation in job.h for when to do dynamic allocation
 */
char *job_to_str(job_t *job, char *str) {
    if (!job || strnlen(job->label, MAX_NAME_SIZE) != MAX_NAME_SIZE - 1) { return NULL; }
    if (!str) { str = malloc(JOB_STR_SIZE); } 
    snprintf(str, JOB_STR_SIZE, JOB_STR_FMT, job->pid, job->id, job->priority, job->label);
    return str;
}

/*
 * TODO: you must implement this function.
 * Hint:
 * - see the hint for job_to_str
 */
job_t *str_to_job(char *str, job_t *job) {
    if (!str || strnlen(str, JOB_STR_SIZE) != JOB_STR_SIZE - 1) { return NULL; }

    pid_t pid;
    unsigned int id;
    unsigned int priority;
    char label[MAX_NAME_SIZE];
    int valid_format = sscanf(str, JOB_STR_FMT, &pid, &id, &priority, label);

    if (valid_format != 4 || strnlen(label, MAX_NAME_SIZE) != MAX_NAME_SIZE - 1) { return NULL; }
    job = (job) ? job_set(job, pid, id, priority, label) : job_new(pid, id, priority, label); 

    return job;
}

/*
 * TODO: you must implement this function
 * Hint:
 * - look at the allocation of a job in job_new
 */
void job_delete(job_t *job) {
    if (job) { free(job); }
}
